Java Kodutöö Teet Adamson IT Kolledž


Lihtne ja konkreete sõnade järjekorra programm Inglise keeles. Idee tekkis koolist, kus sõnade järjekordade harjutusi oleks võinud teha arvutis, mitte paberil.

Programmil on 10 taset, mis viimase läbimisel õnnitleb kasutajat.

Programmil tekstiala, kuhu tuleb sisestada lause suure algustähe ja punktiga.

Kasutatud on Derek Banas tutoriali youtubest Swingi kohta, eelkõige Jpaneli loomisel. Tingimused ja if laused on enda loodud.

Puudu on akna sulgumise käsk uue akna avamisel. Kommentaarid on välja toodud minu enda abiks.

Java Kodutöö Teet Adamson IT Kolledž